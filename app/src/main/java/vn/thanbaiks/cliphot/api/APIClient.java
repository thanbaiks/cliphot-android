package vn.thanbaiks.cliphot.api;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Project: ClipHot
 * Created by Bách on 27/07/2015.
 * Homepage: http://thanbaiks.mobi/
 */
public class APIClient {

    private static OkHttpClient client = new OkHttpClient();


    public static String getContent(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

}

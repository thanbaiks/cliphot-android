package vn.thanbaiks.cliphot;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nineoldandroids.animation.ObjectAnimator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import vn.thanbaiks.cliphot.api.APIClient;
import vn.thanbaiks.cliphot.data.ListPost;
import vn.thanbaiks.cliphot.data.Post;

/**
 * Created by NgoBach on 7/29/2015.
 */

public class ListFragment extends Fragment implements AdapterView.OnItemClickListener {
    private static final String ARG_CAT_ID = "cat_id";
    private static final String ARG_IS_NEW = "hot";
    private static final int PRELOAD_COUNT = 10;
    private boolean mNew;
    private int mCatID;
    private int mPageToLoad;
    private boolean mLoadedAll;
    private boolean mBusy;
    private ListView mListView;
    private MyAdapter mAdapter;
    private boolean mIsDetroyed;

    public static ListFragment getInstance(int catid, boolean isNew) {
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_CAT_ID, catid);
        bundle.putBoolean(ARG_IS_NEW, isNew);
        ListFragment fragment = new ListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNew = getArguments().getBoolean(ARG_IS_NEW);
        mCatID = getArguments().getInt(ARG_CAT_ID);
        mPageToLoad = 1;
        mLoadedAll = false;
        mBusy = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
        mListView = (ListView) rootView.findViewById(R.id.fm_list_list);
        mAdapter = new MyAdapter();
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
        mIsDetroyed = false;
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIsDetroyed = true;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hideAll();
        view.findViewById(R.id.common_loader).setVisibility(View.VISIBLE);
        loadPage();
    }

    private void loadPage() {
        mBusy = true;
        if (mPageToLoad > 1) {
            View loadingView = getView().findViewById(R.id.fm_list_loading_text);
            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 42, getResources().getDisplayMetrics());
            ObjectAnimator anim = new ObjectAnimator().ofFloat(loadingView, "translationY", height, 0);
            anim.setDuration(200);
            anim.start();
        }
        String url = getString(R.string.server) + "/c/" + (mNew ? "new/" : "hot/") + mCatID + "/" + mPageToLoad;
        new Fetcher().execute(url);
        mPageToLoad++;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Post post = (Post) mAdapter.getItem(i);
        post.view++;
        Intent intent = new Intent(getActivity(), PlayerActivity.class);
        intent.putExtra(PlayerActivity.EXTRA_POST, new Gson().toJson(post));
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_bottom, R.anim.scale_out);
    }


    public void hideAll() {
        View rootView = getView();
        rootView.findViewById(R.id.fm_list_list).setVisibility(View.GONE);
        rootView.findViewById(R.id.common_modal).setVisibility(View.GONE);
        rootView.findViewById(R.id.common_loader).setVisibility(View.GONE);
    }

    public void onError(String msg) {
        hideAll();
        View v = getView().findViewById(R.id.common_modal);
        ((TextView) v.findViewById(R.id.common_modal_title)).setText(msg);
        v.setVisibility(View.VISIBLE);
    }

    public void onSuccess(List<Post> posts) {
        if (posts == null || posts.size() == 0) {
            // empty response
            mLoadedAll = true;
            return;
        }
        mAdapter.append(posts);
        hideAll();
        mListView.setVisibility(View.VISIBLE);
    }

    public class Fetcher extends AsyncTask<String, Void, ListPost> {

        @Override
        protected ListPost doInBackground(String... strings) {
            String url = strings[0];
            try {
                String res = APIClient.getContent(url);
                ListPost json = new Gson().fromJson(res, ListPost.class);
                return json;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(ListPost listPost) {
            super.onPostExecute(listPost);
            if (mIsDetroyed)
                return;
            mBusy = false;
            View loadingView = getView().findViewById(R.id.fm_list_loading_text);
            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 42, getResources().getDisplayMetrics());
            new ObjectAnimator().ofFloat(loadingView, "translationY", 0, height)
                    .setDuration(200)
                    .start();
            if (listPost == null) {
                onError("Lỗi không xác định!");
            } else if (listPost.err != null) {
                onError(listPost.err);
            } else {
                // success
                onSuccess(listPost.data);
            }
        }
    }

    public class MyAdapter extends BaseAdapter {

        private List<Post> mPosts;


        public MyAdapter() {
            super();
            mPosts = new ArrayList<>();
        }

        @Override
        public int getCount() {
            return mPosts.size();
        }

        @Override
        public Object getItem(int position) {
            return mPosts.get(position);
        }


        @Override
        public long getItemId(int position) {
            return mPosts.get(position).id;
        }

        public void append(List<Post> l) {
            mPosts.addAll(l);
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View v, ViewGroup parent) {
            ViewHolder viewHolder;
            if (v == null) {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                v = inflater.inflate(R.layout.fragment_list_item, parent, false);
            }
            if (v.getTag() == null) {
                viewHolder = new ViewHolder();
                viewHolder.title = (TextView) v.findViewById(R.id.fm_list_item_title);
                viewHolder.desc = (TextView) v.findViewById(R.id.fm_list_item_desc);
                viewHolder.time = (TextView) v.findViewById(R.id.fm_list_item_time);
                viewHolder.viewCount = (TextView) v.findViewById(R.id.fm_list_item_viewcounter);
                viewHolder.thumbnail = (ImageView) v.findViewById(R.id.fm_list_item_thumbnail);
            } else {
                viewHolder = (ViewHolder) v.getTag();
            }
            viewHolder.title.setText(mPosts.get(position).title);
            viewHolder.desc.setText(mPosts.get(position).text);
            viewHolder.time.setText(mPosts.get(position).time);
            viewHolder.viewCount.setText(mPosts.get(position).view + " lượt");
            Picasso.with(getActivity()).load("http://img.youtube.com/vi/" + mPosts.get(position).youtube_id + "/0.jpg")
                    .into(viewHolder.thumbnail);

            // check if has to load more
            if (!mBusy && !mLoadedAll && getCount() - position - 1 < PRELOAD_COUNT) {
                loadPage();
            }
            return v;
        }

        private class ViewHolder {
            public TextView title;
            public TextView desc;
            public TextView time;
            public TextView viewCount;
            public ImageView thumbnail;
            public ImageView fav;
        }
    }
}

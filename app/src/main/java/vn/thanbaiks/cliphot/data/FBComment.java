package vn.thanbaiks.cliphot.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bach on 8/11/2015.
 */
public class FBComment {
    public String created_time;
    @SerializedName("from")
    public FBID owner;
    public String message;
    public int like_count;

    @Override
    public String toString() {
        return owner.name + ": " + message + " (" + like_count + ")";
    }
}

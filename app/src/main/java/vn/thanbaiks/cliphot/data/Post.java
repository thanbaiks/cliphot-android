package vn.thanbaiks.cliphot.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NgoBach on 7/29/2015.
 */
public class Post {
    public int id;
    public String title;
    public String text;
    @SerializedName("yt")
    public String youtube_id;
    public int view;
    public String time;
    public String url;
}

package vn.thanbaiks.cliphot.data;

/**
 * Project: ClipHot
 * Created by Bách on 27/07/2015.
 * Homepage: http://thanbaiks.mobi/
 */
public class Cat {
    public int id;
    public String title;
    public int count;

    public Cat(int i, String t, int c) {
        id = i;
        title = t;
        count = c;
    }
}

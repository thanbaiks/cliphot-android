package vn.thanbaiks.cliphot.data;

import java.util.List;

import vn.thanbaiks.cliphot.api.BaseJSON;

/**
 * Project: ClipHot
 * Created by Bách on 27/07/2015.
 * Homepage: http://thanbaiks.mobi/
 */
public class CatJSON extends BaseJSON {
    public List<Cat> data;
}
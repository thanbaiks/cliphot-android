package vn.thanbaiks.cliphot.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NgoBach on 8/8/2015.
 */
public class FBUrlInfo {
    @SerializedName("like_count")
    public int likes;
    @SerializedName("comment_count")
    public int comments;
    @SerializedName("share_count")
    public int shares;
}

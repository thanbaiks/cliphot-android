package vn.thanbaiks.cliphot.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Bach on 8/13/2015.
 */
public class Preferences {
    private static String KEY_AUTOPLAY = "autoplay";

    private SharedPreferences preferences;

    private Preferences(Context ctx) {
        preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static Preferences from(Context ctx) {
        return new Preferences(ctx);
    }

    public boolean autoplay() {
        return preferences.getBoolean(KEY_AUTOPLAY, true);
    }

    public void setAutoplay(boolean b) {
        preferences.edit().putBoolean(KEY_AUTOPLAY, b).apply();
    }
    public void purge(){
        preferences.edit().clear().apply();
    }
}

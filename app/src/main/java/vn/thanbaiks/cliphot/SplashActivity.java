package vn.thanbaiks.cliphot;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;

import java.io.IOException;

import vn.thanbaiks.cliphot.api.APIClient;
import vn.thanbaiks.cliphot.data.CatJSON;


public class SplashActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ImageView imgView = (ImageView) findViewById(R.id.splash_img);
        ValueAnimator anim = ObjectAnimator.ofFloat(imgView, "alpha", 1, 0.2f);
        anim.setDuration(1000);
        anim.setRepeatCount(ValueAnimator.INFINITE);
        anim.setRepeatMode(ValueAnimator.REVERSE);
        anim.start();

        String url = getResources().getString(R.string.server) + "/c";
        new CatFetcher().execute(url);

    }

    @Override
    public void onBackPressed() {
        // ignore back key
    }


    private class CatFetcher extends AsyncTask<String, Void, CatJSON> {
        @Override
        protected CatJSON doInBackground(String... params) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {}
            String url = params[0];
            Gson gson = new Gson();
            CatJSON catList = null;
            try {
                String tmp = APIClient.getContent(url);
                Log.d("WTF", tmp);
                catList = gson.fromJson(APIClient.getContent(url), CatJSON.class);
                return catList;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(CatJSON result) {
            if (result == null || result.err != null) {
                new AlertDialog.Builder(SplashActivity.this)
                        .setTitle("Lỗi")
                        .setMessage(result == null ? "Không thể kết nối đến máy chủ" : result.err)
                        .setPositiveButton("Xác nhận", null)
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                finish();
                            }
                        })
                        .show();
            } else {
                // continue to main activity
                String param = new Gson().toJson(result.data);
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                intent.putExtra(getString(R.string.param_categories), param);
                startActivity(intent);
                finish();
            }
        }
    }
}

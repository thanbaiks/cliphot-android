package vn.thanbaiks.cliphot;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.squareup.picasso.Picasso;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnLogoutListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.utils.PictureAttributes;

import java.util.ArrayList;
import java.util.List;

import vn.thanbaiks.cliphot.data.Cat;
import vn.thanbaiks.cliphot.util.Preferences;


public class MainActivity extends ActionBarActivity implements ActionBar.TabListener, View.OnClickListener {

    private static String PREF_USER_NAME = "user.name";
    private static String PREF_USER_PICTURE = "user.picture";
    public SimpleFacebook mSimpleFacebook;
    private int mCurrentCat;
    private ViewPager mViewPager;
    private SlidingMenu mSlidingMenu;
    private InterstitialAd mAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);

        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }
        // create sliding mSlidingMenu
        mSlidingMenu = new SlidingMenu(this);
        mSlidingMenu.setMode(SlidingMenu.LEFT);
        mSlidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        mSlidingMenu.setShadowDrawable(R.drawable.slidingmenu_shadow);
        mSlidingMenu.setShadowWidthRes(R.dimen.slidingmenu_shadow_width);
        mSlidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_shadow_behind_offet);
        mSlidingMenu.setFadeDegree(0.35f);
        mSlidingMenu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
        mSlidingMenu.setMenu(R.layout.activity_main_slidingmenu);
        mSlidingMenu.setOnOpenListener(new SlidingMenu.OnOpenListener() {
            @Override
            public void onOpen() {
                actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_left);
            }
        });
        mSlidingMenu.setOnCloseListener(new SlidingMenu.OnCloseListener() {
            @Override
            public void onClose() {
                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            }
        });


        /**
         * List categories
         */
        initCategories();

        /**
         * Initialize side option menu
         */
        initSideMenu();

        /**
         * Admob
         */
        AdView mAdView = (AdView) findViewById(R.id.admob_main);
        mAdView.loadAd(Ad.getAdRequest());
        mAd = new InterstitialAd(this);
        mAd.setAdUnitId(getString(R.string.admob_before_exit_ad_id));
        mAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                finish();
            }
        });
        mAd.loadAd(Ad.getAdRequest());

        initFB();
        /**
         * handle facebook button click event
         */
        findViewById(R.id.btn_user).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSimpleFacebook != null) {
                    if (mSimpleFacebook.isLogin()) {
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Đã đăng nhập")
                                .setMessage("Bạn có muốn đăng xuất không?")
                                .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        mSimpleFacebook.logout(new OnLogoutListener() {
                                            @Override
                                            public void onLogout() {
                                                Toast.makeText(MainActivity.this,
                                                        "Xin vui lòng thoát ra và đăng nhập lại", Toast.LENGTH_SHORT)
                                                        .show();
                                            }
                                        });

                                    }
                                })
                                .setNegativeButton("Không", null)
                                .show();
                    } else {
                        mSimpleFacebook.login(new OnLoginListener() {
                            @Override
                            public void onLogin(String token, List<Permission> accepted, List<Permission> declined) {
                                loadFacebookProfile();
                            }

                            @Override
                            public void onCancel() {

                            }

                            @Override
                            public void onException(Throwable throwable) {
                                throwable.printStackTrace();
                            }

                            @Override
                            public void onFail(String reason) {
                                Toast.makeText(
                                        MainActivity.this,
                                        "Login failed\n" + reason,
                                        Toast.LENGTH_LONG
                                ).show();
                            }
                        });
                    }
                } else {
                    Log.e("Facebook", "FB is null?!");
                }
            }
        });

        /**
         * Auto Load facebook profile
         */
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadFacebookProfile();
            }
        }, 500);
    }

    /**
     * Require exit
     * Show interstitial ad and quit
     */
    public void requireExit(){
//        if (mAd.isLoaded()){
//            mAd.show();
//        }else{
            finish();
//        }
    }

    /**
     * Get user name and profile pictue
     * Load into left sliding menu views
     */
    private void loadFacebookProfile() {
        PictureAttributes attributes = PictureAttributes.createPictureAttributes();
        attributes.setHeight(256);
        attributes.setWidth(256);
        attributes.setType(PictureAttributes.PictureType.SQUARE);
        Profile.Properties properties = new Profile.Properties.Builder()
                .add(Profile.Properties.NAME)
                .add(Profile.Properties.PICTURE, attributes)
                .add(Profile.Properties.COVER)
                .build();
        final TextView textView = (TextView) findViewById(R.id.slidingmenu_user_title);
        final ImageView imageView = (ImageView) findViewById(R.id.slidingmenu_user_picture);
        mSimpleFacebook.getProfile(properties, new OnProfileListener() {
            @Override
            public void onComplete(Profile response) {
                textView.setText(String.format(getString(R.string.hello_user), response.getName()));
                Picasso.with(MainActivity.this)
                        .load(response.getPicture())
                        .into(imageView);
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                prefs.edit()
                        .putString(PREF_USER_NAME, response.getName())
                        .putString(PREF_USER_PICTURE, response.getPicture())
                        .apply();
                Log.i("Facebook", "Loaded from response");
                Log.i("Facebook", "Profile picture: " + response.getPicture());
            }

            @Override
            public void onFail(String reason) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                String userName = prefs.getString(PREF_USER_NAME, null);
                String userPicture = prefs.getString(PREF_USER_PICTURE, null);
                if (userName != null) {
                    textView.setText(String.format(getString(R.string.hello_user), userName));
                }
                if (userPicture != null) {
                    Picasso.with(MainActivity.this)
                            .load(userPicture)
                            .into(imageView);
                }
                Log.i("Facebook", "Loaded from preferences, reason: " + reason);
            }
        });
    }

    /**
     * Initialize facebook sdk
     */
    private void initFB() {
        Permission[] permissions = new Permission[]{
                Permission.USER_PHOTOS,
                Permission.EMAIL,
                Permission.PUBLISH_ACTION
        };
        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
                .setAppId(getString(R.string.fb_app_id))
                .setNamespace(getString(R.string.fb_app_ns))
                .setPermissions(permissions)
                .build();
        SimpleFacebook.setConfiguration(configuration);

        mSimpleFacebook = SimpleFacebook.getInstance(this);
    }

    /**
     * Initialize Categories
     */
    private void initCategories() {
        Gson gson = new Gson();
        List<Cat> mCats = new ArrayList<>();
        String param = getString(R.string.param_categories);
        if (getIntent().hasExtra(param)) {
            param = getIntent().getStringExtra(param);
            mCats = gson.fromJson(param, new TypeToken<List<Cat>>() {
            }.getType());
        }
        mCats.add(0, new Cat(0, "Tất cả", 0));
        mCurrentCat = 0;
        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.menu_container);
        for (Cat c : mCats) {
            View v = new MenuItemBuilder(this)
                    .setTitle(c.title)
                    .setLabel(c.count > 0 ? "" + c.count : "")
                    .setTag(c.id)
                    .setIcon(R.drawable.ic_youtube_red)
                    .setListener(this)
                    .build();
            viewGroup.addView(v);
        }
    }

    private void initSideMenu() {
        ViewGroup container = (ViewGroup) findViewById(R.id.menu_options_container);
        View v;

        // Preferences
        final Preferences preferences = Preferences.from(getApplicationContext());
        v = new MenuItemBuilder(this)
                .setTitle(getString(R.string.settings))
                .setIcon(R.drawable.ic_gear)
                .setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSlidingMenu.showContent();
                        // Show preferences dialog
                        final AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this)
                                .setTitle(R.string.settings)
                                .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface d, int which) {
                                        // Save all settings
                                        preferences.setAutoplay(((SwitchCompat) ((AlertDialog)d).findViewById(R.id.pref_autoplay)).isChecked());
                                    }
                                })
                                .setNeutralButton(getString(R.string.reset), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Just remove all settings
                                        preferences.purge();
                                    }
                                })
                                .setNegativeButton(getString(android.R.string.cancel), null);

                        final View prefView = LayoutInflater.from(dialog.getContext()).inflate(R.layout.preferences, null,false);
                        // Find and load settings to alert dialog
                        ((SwitchCompat)prefView.findViewById(R.id.pref_autoplay)).setChecked(preferences.autoplay());
                        dialog.setView(prefView);
                        dialog.show();
                    }
                })
                .build();
        container.addView(v);

        // Facebook Fanpage
        v = new MenuItemBuilder(this)
                .setTitle(getString(R.string.facebook_fanpage))
                .setIcon(R.drawable.ic_rating)
                .setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("fb://page/"+ getString(R.string.fanpage_id)));
                            startActivity(intent);
                        } catch (Exception e) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://touch.facebook.com/" + getString(R.string.fanpage_id)));
                            startActivity(intent);
                        }
                    }
                })
                .build();
        container.addView(v);

        // Đến trang chủ
        v = new MenuItemBuilder(this)
                .setTitle(getString(R.string.go_home_page))
                .setIcon(R.drawable.ic_home)
                .setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mSlidingMenu.showContent();
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.home_page)));
                        startActivity(intent);
                    }
                })
                .build();
        container.addView(v);

        // Info
        v = new MenuItemBuilder(this)
                .setTitle(getString(R.string.about))
                .setIcon(R.drawable.ic_info)
                .setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mSlidingMenu.showContent();
                        showAboutDialog();
                    }
                })
                .build();
        container.addView(v);

        // Exit
        v = new MenuItemBuilder(this)
                .setTitle(getString(R.string.exit))
                .setIcon(R.drawable.ic_shutdown)
                .setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        requireExit();
                    }
                })
                .build();
        container.addView(v);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }


    public int getCurrentCategory() {
        return mCurrentCat;
    }

    /**
     * Display about dialog about this application
     */
    public void showAboutDialog(){
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.about))
                .setMessage(getString(R.string.about))
                .setMessage(Html.fromHtml(getString(R.string.about_mess)))
                .setPositiveButton(getString(android.R.string.ok),null)
                .create();
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        if (mSlidingMenu.isMenuShowing()) {
            mSlidingMenu.showContent();
        } else {
            requireExit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mSlidingMenu.toggle();
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        mSlidingMenu.showContent();
        mCurrentCat = (int) v.getTag();
        mViewPager.getAdapter().notifyDataSetChanged();
        mViewPager.invalidate();
        mViewPager.setCurrentItem(0);
        TextView textView = (TextView) v.findViewById(R.id.menu_item_title);
        setTitle(textView.getText());
    }

    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ListFragment.getInstance(getCurrentCategory(), position == 0);
        }

        @Override
        public int getCount() {
            return 2;
        }

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Clip Mới";
                case 1:
                    return "Clip Hot";
                default:
                    return "Oops";
            }
        }
    }

    public static class MenuItemBuilder {
        private Context ctx;
        private View.OnClickListener listener;
        private String title, label;
        private Object tag;
        private int resDrawable;

        public MenuItemBuilder(Context ctx) {
            this.ctx = ctx;
            title = "";
            label = "";
            listener = null;
            tag = null;
            resDrawable = 0;
        }

        public MenuItemBuilder setTitle(String s) {
            title = s;
            return this;
        }

        public MenuItemBuilder setLabel(String s) {
            label = s;
            return this;
        }

        public MenuItemBuilder setIcon(int resID) {
            resDrawable = resID;
            return this;
        }

        public MenuItemBuilder setListener(View.OnClickListener listener) {
            this.listener = listener;
            return this;
        }

        public MenuItemBuilder setTag(Object tag) {
            this.tag = tag;
            return this;
        }

        public View build(ViewGroup container) {
            View view = LayoutInflater.from(ctx).inflate(R.layout.activity_main_slidingmenu_menuitem, container, false);
            ((TextView) view.findViewById(R.id.menu_item_label)).setText(label);
            ((TextView) view.findViewById(R.id.menu_item_title)).setText(title);
            if (label.equals(""))
                view.findViewById(R.id.menu_item_label).setVisibility(View.INVISIBLE);
            if (listener != null) {
                view.setOnClickListener(listener);
            }
            if (tag != null) {
                view.setTag(tag);
            }
            if (resDrawable > 0) {
                ((TextView) view.findViewById(R.id.menu_item_title)).setCompoundDrawablesWithIntrinsicBounds(resDrawable, 0, 0, 0);
            }
            return view;
        }

        public View build() {
            return build(null);
        }
    }
}


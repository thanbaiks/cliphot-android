package vn.thanbaiks.cliphot;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import vn.thanbaiks.cliphot.api.APIClient;
import vn.thanbaiks.cliphot.data.FBComment;
import vn.thanbaiks.cliphot.data.FBUrlComments;
import vn.thanbaiks.cliphot.data.FBUrlInfo;
import vn.thanbaiks.cliphot.data.Post;
import vn.thanbaiks.cliphot.util.Preferences;

/**
 * Created by NgoBach on 7/30/2015.
 */
public class PlayerActivity extends YouTubeBaseActivity implements OnInitializedListener, YouTubePlayer.OnFullscreenListener, View.OnClickListener {

    public static String EXTRA_POST = "vn.thanbaiks.cliphot.PlayerActivity.post";
    private static int RECOVERY_DIALOG_REQUEST = 1;

    private YouTubePlayerView playerView;
    private YouTubePlayer player;
    private Post post;
    private boolean isFullScreen;
    private boolean isDestroyed;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_player);
        playerView = (YouTubePlayerView) findViewById(R.id.player);
        playerView.initialize(getString(R.string.youtube_api_key), this);

        post = new Gson().fromJson(getIntent().getStringExtra(EXTRA_POST), Post.class);
        ((TextView) findViewById(R.id.player_title)).setText(post.title);
        ((TextView) findViewById(R.id.player_desc)).setText(post.text);
        ((TextView) findViewById(R.id.player_time)).setText(post.time);
        ((TextView) findViewById(R.id.player_view)).setText(""+post.view);

        // admob initialize
        AdView mAdView = (AdView) findViewById(R.id.admob_player);
        mAdView.loadAd(Ad.getAdRequest());

        // increase view count
        new Thread() {
            @Override
            public void run() {
                try {
                    APIClient.getContent(getString(R.string.server) + "/p/" + post.id);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(PlayerActivity.this, "Lỗi mạng!", Toast.LENGTH_SHORT).show();
                }
            }
        }.start();

        // Handle buttons click
        findViewById(R.id.player_btn_share).setOnClickListener(this);
        findViewById(R.id.player_btn_share_fb).setOnClickListener(this);
        findViewById(R.id.player_btn_write_comment).setOnClickListener(this);


        isDestroyed = false;
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        player = youTubePlayer;
        player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE | YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);
        youTubePlayer.setOnFullscreenListener(this);
        Preferences preferences = Preferences.from(getApplicationContext());
        if (!wasRestored) {
            if (preferences.autoplay())
                youTubePlayer.loadVideo(post.youtube_id);
            else
                youTubePlayer.cueVideo(post.youtube_id);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(post.url));
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            playerView.initialize(getString(R.string.youtube_api_key), this);
        }
    }

    @Override
    public void onBackPressed() {
        if (isFullScreen) {
            player.setFullscreen(false);
            return;
        }
        super.onBackPressed();
        overridePendingTransition(R.anim.scale_in, R.anim.slide_out_bottom);
    }

    @Override
    protected void onDestroy() {
        isDestroyed = true;
        super.onDestroy();
    }

    @Override
    public void onFullscreen(boolean b) {
        isFullScreen = b;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.player_btn_share) {
            // share action
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, post.title);
            intent.putExtra(Intent.EXTRA_TEXT, post.url);
            startActivity(Intent.createChooser(intent, "Chia sẻ với"));
        }else if (v.getId() == R.id.player_btn_share_fb){
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, post.title);
            intent.putExtra(Intent.EXTRA_TEXT, post.url);
            List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent,0);
            boolean found = false;
            for (ResolveInfo info : matches){
                if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")){
                    intent.setPackage(info.activityInfo.packageName);
                    found = true;
                    break;
                }
            }
            if (found){
                startActivity(intent);
            }else{
                Toast.makeText(this,"Facebook chưa được cài đặt!",Toast.LENGTH_SHORT).show();
            }
        } else if (v.getId() == R.id.player_btn_write_comment){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(post.url + "#comment"));
            startActivity(intent);
        }
    }
}

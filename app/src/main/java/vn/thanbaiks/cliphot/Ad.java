package vn.thanbaiks.cliphot;

import com.google.android.gms.ads.AdRequest;

/**
 * Created by NgoBach on 7/31/2015.
 */
public class Ad {
    public static AdRequest getAdRequest(){
        return new AdRequest.Builder()
                .addTestDevice("C37927BC826AA4266459E90D2D064422")
                .addTestDevice("BE7FDE9C7226F8281199FB620F68A7DF")
                .addTestDevice("4147A7A20041A8CC3E4AC0B11A524FF7")
                .build();
    }
}
